# Practice Exercises for Selenium

The exercises are here: https://docs.google.com/document/d/1ERCvuaswPGe0qXk4ZxUDYahFNbh4paA4hgXz6TJNfSs/edit#

## How to Use

1. Download the repository
2. In Eclipse, start new Java project
3. Add Selenium libraries (JAR) files to the project
4. Add files from repository into your Java project
5. Done!


