import static org.junit.Assert.*;

import java.util.concurrent.TimeUnit;

import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class WarmupExercisesTest {
	
	protected final static String CHROME_DRIVER_PATH = "/Users/parrot/Downloads/chromedriver";
	protected static WebDriver driver;
	
	/**
	 * Run this function BEFORE all the test cases.
	 * Similar to viewDidLoad() or onCreate() in Swift/Android
	 * @throws Exception
	 */
	@BeforeClass
	public static void setup() throws Exception {

		// Start Chrome
		System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		
		// Set the "waiting period" between each command
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	/** 
	 * Run this function AFTER all the test cases.
	 * @throws Exception
	 */
	@AfterClass
	public static void tearDown() throws Exception {
		driver.quit();
	}
	
	// Warmup exercises!
	// ------------------------------
	
	/**
	 * Visit any webpage
	 * Find an <a href=""> link
	 * Get the url
	 * Output the url to Terminal
	 * @throws InterruptedException 
	 */
	@Test
	public void testGettingALink() throws InterruptedException {
		fail("Not implemented"); 
	}

	
	/**
	 * Visit any webpage
	 * Find an <img> tag
	 * Get the img src
	 * Output src to the Terminal
	 */
	@Test
	public void testImageSrc() {
		fail("Not implemented");
	}
	
	/**
	 * Go here: https://longform.org/lists/best-of-2018
	 * Get the article titles and output to screen
	 * Sample output:
	 * 	- Japan's Rent-a-Family Industry
	 *  - Trapped by the 'Walmart of Heroin'
	 *  - Trashed: Inside the Deadly World of Private Garbage Collection
	 *  - etc, etc
	 */
	@Test
	public void testGetGroupOfElements() {
		fail("Not implemented");
	}
	

	
}
