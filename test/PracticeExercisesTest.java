import static org.junit.jupiter.api.Assertions.*;

import java.util.concurrent.TimeUnit;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

class PracticeExercisesTest {

	protected final static String CHROME_DRIVER_PATH = "/Users/parrot/Downloads/chromedriver";
	protected static WebDriver driver;
	
	@BeforeAll
	static void setUpBeforeClass() throws Exception {
		// Start Chrome
		System.setProperty("webdriver.chrome.driver", CHROME_DRIVER_PATH);
		driver = new ChromeDriver();
		
		// Set the "waiting period" between each command
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	@AfterAll
	static void tearDownAfterClass() throws Exception {
		driver.quit();
	}

	@Test
	void testExercise1() {
		fail("Not yet implemented");
	}
	
	@Test
	void testExercise2() {
		fail("Not yet implemented");
	}
	
	@Test
	void testExercise3() {
		fail("Not yet implemented");
	}

	@Test
	void testExercise4() {
		fail("Not yet implemented");
	}
	
	@Test
	void testExercise5() {
		fail("Not yet implemented");
	}
}
